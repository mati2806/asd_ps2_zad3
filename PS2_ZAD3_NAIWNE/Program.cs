﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;
using System.Threading;



namespace PS2_ZAD3_NAIWNE
{

    class Program
    {
        static int waterSum = 0;
        static int iterations = 0;

        static void Main(string[] args)
        {
            string fileName = "in6.txt"; //wybór testu od in1 do in6
            int[,,] cells = new int[,,] { };
            int sizeX=0;
            int sizeY=0;
            int startX = 0;
            int startY = 0;

            var watch = System.Diagnostics.Stopwatch.StartNew();

            try { LoadFile(fileName, ref sizeX, ref sizeY, ref startX, ref startY, ref cells); }
            catch (FileNotFoundException)
            {
                Console.WriteLine("Nie znaleziono pliku!");
                return;
            }

            CheckCells(startX, startY, startX, startY, ref cells);
            SaveFile();
            
            //ReadHashTable(sizeX, sizeY,ref cells);
            Console.WriteLine("Odpowiedz to " + waterSum);
            Console.WriteLine("Ilosc iteracji: " + iterations);

        }

        public static void LoadFile(string fileName, ref int sizeX, ref int sizeY, ref int startX, ref int startY,  ref int[,,] cells)
        {
            int i = 0;
            int j = 0;
            int k = 2;
            string[] words = new string[] { };

            StreamReader reader = new StreamReader(fileName);

            words = reader.ReadToEnd().Split(new[] { '\n', ' ' });

            sizeX = Convert.ToInt32(words[0]);
            sizeY = Convert.ToInt32(words[1]);

            cells= new int[sizeX+2, sizeY+2, 2];

            startX = Convert.ToInt32(words[words.Length - 3]);
            startY = Convert.ToInt32(words[words.Length - 2]);


            for(i=0; i<sizeX+2; i++)
            {
                for(j=0; j<sizeY+2; j++)
                {
                    if (i == 0 || j == 0 || i == sizeX + 1 || j == sizeY + 1) cells[i, j, 0] = 0;
                    else
                    {
                        cells[i, j, 0] = Convert.ToInt32(words[k]);
                        k++;
                    }

                    //Console.Write(cells[i, j, 0]);
                }
                //Console.WriteLine();
            }

            Console.WriteLine("Size: " + sizeX + "x" + sizeY);
            Console.WriteLine("Start: (" + startX + "," + startY + ")");
            Console.WriteLine();


         
        }

        public static void SaveFile()
        {
            StreamWriter writer = new StreamWriter("out.txt");
            writer.WriteLine(waterSum);
            writer.Close();
        }

        public static void CheckCells(int X, int Y, int startX, int startY, ref int[,,] cells)
        {
            int i = 0;
            int j = 0;
            
            cells[X, Y, 1] = 1;

            //Console.WriteLine("Sprawdzam dla (" + Y + "," + X + ") " + cells[X, Y, 0]);

            if ((cells[X, Y, 0] - cells[startX, startY, 0] + 1) > 5)
            {
                waterSum += 5;
                //Console.WriteLine("Dodaje 5");
            }   
            else
            {
                waterSum += (cells[X, Y, 0] - cells[startX, startY, 0]) + 1;
                //Console.WriteLine("Dodaje " + ((cells[X, Y, 0] - cells[startX, startY, 0]) + 1));
            }     

            for (i = -1; i < 2; i++)
            {
                for (j = -1; j<2; j++)
                {
                    if (i == 0 && j == 0) continue;
                    if ((cells[X + i, Y + j, 0] >= cells[startX, startY, 0]) && (cells[X + i, Y + j, 0] >= cells[X, Y, 0] - 4) && (cells[X + i, Y + j, 0] <= cells[X, Y, 0] + 4) && cells[X + i, Y + j, 1] != 1) 
                        CheckCells(X + i, Y + j, startX, startY, ref cells);

                    iterations++;

                }
             
            }


        }

        public static void ReadHashTable(int sizeX, int sizeY,ref int [,,] cells)
        {
            int i;
            int j;

            Console.WriteLine();
            Console.WriteLine("Polaczone cele: ");
            for (i = 0; i < sizeX + 2; i++)
            {
                for (j = 0; j < sizeY + 2; j++)
                {
                    Console.Write(cells[i, j, 1]);
                }
                Console.WriteLine();
            }
        }

        
    }
}
